
    const express = require('express');
    const mongoose =require('mongoose');
    const jwt = require('jsonwebtoken');
    const router = express.Router();
    const path = require('path');
    const cors = require('cors');
    const multer = require('multer');
    const tokenKey = require('../models/user');
    const pModel = require('../models/products');
    const User = mongoose.model('User');
    const Product = mongoose.model('Product');
    const requireToken = require('../middleware/requireToken');
    const nodemailer = require('nodemailer');
    const emailDetails = require('dotenv').config();
    const crypto = require('crypto');


//------------------------------------------------------------------------------
        // Configure how uploadded images should be stored
        const storage = multer.diskStorage({
            destination: './public/uploads/',
            filename: function(req,file,cb){
                cb(null, Date.now() + file.originalname );
            }
        });
        //Function to initialize file upload

        const upload = multer({
        storage: storage
        }).single('profileImage');
        // For initializing product image upload
        const uploadProductImage = multer({
            storage: storage
            }).single('productImage');
 //------------------------------------------------------------------------------


//------------------------------------------------------------------------------
        // Testing authorization token to see if it works
            
        router.get('/',(req,res)=>{
                    res.send({
                        msg: "Welcome to Kiwi Mart"
                    });
        });
 //------------------------------------------------------------------------------


//------------------------------------------------------------------------------      
        //Route for handling signups
            router.post('/signup',  async(req,res)=>{
            
                console.log(req.body);
               
                const {fullName,email, mobileNumber,university, address_1, userName,userProfileImage, password,resetPasswordToken,resetPasswordExpires} = req.body;
                let user = ( await User.findOne({email}));
               
                if(user){
                    res.status(422).send({error: "Email already exists"});
                }
                
                try {
                    let user = new User({fullName,email,mobileNumber,university,address_1,userName,userProfileImage, password, resetPasswordToken,resetPasswordExpires}); 
                await user.save();
                const token = jwt.sign({ userId: user._id }, tokenKey);
                console.log(token);
                res.send({token}); 
                
                

                    //Email sending handler
                const myEmail = process.env.EMAIL;
                const myPassword = process.env.PASSWORD;

                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: myEmail,
                        pass: myPassword
                            },
                            tls: {
                                rejectUnauthorized: false
                            }

                        });

                        const mailOptions = {
                            from: KiwiMart-Team,
                            to: "md@uvitechgh.com",
                            subject: 'Successful Registration',
                            text: `Hello ${fullName}, thank you for using Kiwi-Mart. \n\n`
                                + `Have a delightful experience. \n\n`
                        }

                        transporter.sendMail(mailOptions, function (err, res) {
                            if (err) {
                                console.log("Erro has ocurred", err);
                                
                            }
                            else {
                                console.log("email sent!");
                                


                            }
                        });

            //end of mail handler
















                } catch (err) {
                    res.send({err});
                    
                }   
            }); 
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
        //Route for handling signin
        router.post('/signin',async (req,res,next)=>{
            const {email,userName, password} = req.body;

            if(!(email||userName) || !password){
                res.status(422).send({error: "You must provide email or password"});
            }
            
            else{
                const user = ( await User.findOne({email}) || await User.findOne({userName}));

                if(!user){
                            const log = (error)=>{
                                next(error); 
                                res.status(422).send({error: "User not found in database"});
                            }
                }
                try {
                    await user.comparePassword(password);
                    const token = jwt.sign({userId: user._id }, tokenKey);
                    const userID = user._id;
                    res.send({token,userID}); 
                } catch (err) {
                                
                                res.send({error: "Wrong credentials"});   
                }
            }    
        });
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
        // Edit profile route handler
            router.put('/edit',async(req,res)=>{
                console.log(req.body);

                const {email, mobileNumber, address_1,userID} = req.body;

                const _id = userID
                
                const user =  await User.findOne({_id});

                console.log(user._id);      
                try {
                    
                    const updateEmail= (user.email = email);
                    const updateMobileNumber= (user.mobileNumber = mobileNumber);
                    const updateAddrees_1= (user.address_1 = address_1);
                await user.save();
                const token = jwt.sign({ userId: user._id }, tokenKey);
                res.send({token});  
                } catch (err) {
                    res.status(422).send(err.message);
                }   
            });
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
        //Forgot password route and sending email to user
        router.post('/forgotpassword', async (req, res) => {
            console.log(req.body);

            const { email } = req.body;

            if (!email) {
                res.status(422).send({ error: "You must provide email" });
            }

            const user = await User.findOne({ email });
            if (!user) {

                res.status(422).send({ error: "User not found in database" });
            }
            else {

                const resetToken = crypto.randomBytes(20).toString('hex');
             var   resetPasswordToken = resetToken;

                try {
                    const insertResetPasswordTokenToDatabse = await User.updateOne({ email }, {
                        resetPasswordToken: resetPasswordToken,
                        resetPasswordExpires: Date.now() + 3600000
                    });
                    res.send({ msg: "Verification code has been sent to your email!" });
                } catch (error) {
                    res.status(422).send(error.message);
                }

            //Email sending handler
                const myEmail = process.env.EMAIL;
                const myPassword = process.env.PASSWORD;

                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: myEmail,
                        pass: myPassword
                            },
                            tls: {
                                rejectUnauthorized: false
                            }

                        });

                        const mailOptions = {
                            from: "KiwiMart-Team",
                            to: "md@uvitechgh.com",
                            subject: 'Password Reset Verification Code',
                            text: `Hello ${user.userName}, you requested to reset your password. Below is the verification code to reset your password. \n\n`
                                + `${resetToken} \n\n`
                        }

                        transporter.sendMail(mailOptions, function (err, res) {
                            if (err) {
                                console.log("Erro has ocurred", err);
                                
                            }
                            else {
                                console.log("email sent!");
                                


                            }
                        });

            //end of mail handler
            }

        });
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
            //Update password via reset link handler
        router.post('/reset', async (req, res) => {
            console.log(req.body);
            const {resetToken, password } = req.body;

            let user = await User.findOne({

                resetPasswordToken: resetToken,
                resetPasswordExpires: {
                    $gt: Date.now()
                }
            });

            if (!user) {
                console.log("Password reset code is invalid or expired");
                res.json({error: 'Password reset code is invalid or expired'});
            }

            else {
                // let user = await User.findOne({email});

                console.log("User found eeeeeeh");

                try {

                    const updatePassword = (user.password = password);
                    const updateResetPasswordToken = (user.resetPasswordToken = '');
                    const updateResetPasswordExpires = (user.resetPasswordExpires = null);

                    await user.save();
                    const token = jwt.sign({ userId: user._id }, tokenKey);
                    res.send({
                        token,
                        userID: user._id,
                        msg: 'pasword changed!'
                    });

                } catch (error) {
                    res.status(422).send(error.message);
                }
            }
        });
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
    //Pulling user information from the databse, using the user token generated

    router.get('/profile', requireToken,(req,res)=>{
        res.send({
            fullName:req.user.fullName,
            email:req.user.email,
            userId: req.user._id,
            university: req.user.university,
            userName:req.user.userName,
            mobileNumber:req.user.mobileNumber,
            address_1: req.user.address_1,
            profileImage:req.user.userProfileImage
        });
});
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Route for showing the users profile imae
router.get('/image', requireToken,(req,res)=>{
    res.send({
        email:req.user.email,
        profileImage:req.user.profileImage   
    });
});
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Route to handle profile image upload
router.post('/editProfile',  async(req,res)=>{
    
    upload(req,res, async(err)=>{
                
                
        if(err){
            console.log("this is error",err);
        }
        
        console.log('this file path',req.file.path);
        const profileImage = "http://192.168.42.114:3000/uploads/"+req.file.filename || "";

       const email = req.body.email;
        const mobileNumber = req.body.mobileNumber;
        const address_1 = req.body.address_1;
    
        const _id = req.body.userID;
                
        const user =  await User.findOne({_id});   

            
            try {
                const updateEmail= (user.email = email);
                const updateMobileNumber= (user.mobileNumber = mobileNumber);
                const updateAddrees_1= (user.address_1 = address_1);
                const updateProfileImage = (user.userProfileImage = profileImage);
                await user.save();
                const token = jwt.sign({ userId: user._id }, tokenKey);
                console.log("Done");
                res.send({token,msg:"Updated"}); 

                }
                
             catch (err) {

                res.status(422).send(err.message);
               
            } 
    });

}); 
//------------------------------------------------------------------------------

// Add Product Route
router.post('/addProduct', async(req,res)=>{
    uploadProductImage(req,res, async(err)=>{
        if(err){
            console.log("this is error",err);
        }
        else{
            console.log("file uploaded");
            
        
        }
        console.log('this file path',req.file.path);
        
        const imagePath = "http://192.168.42.114:3000/uploads/"+req.file.filename || "";
        //"https://warm-ridge-56934.herokuapp.com/uploads/"+req.file.filename;
    
        const title = req.body.title;
    
        const description = req.body.description;
        const price = req.body.price;
        const category = req.body.category;
        const contact = req.body.contact;
        const whatsapp = req.body.whatsapp;
        const userID = req.body.userID
        

        const product = {category:category, title:title,description:description,price:price,images:imagePath, contact:contact, whatsapp:whatsapp, dateCreated:new Date(),
        _user:userID
    }

    
    try {
        Product.create(product, function(err,products){
            if(err){throw err;}
            res.json(products);
        });
        
    } catch (error) {

        res.status(422).send(error.message);
    }
    
    
});
    
    
    

});

//Fetching products

router.get('/getProduct', async(req,res)=>{
    Product.find(function(err,products){
        if(err){throw err;}
        res.json(products);
    });


});

router.post('/getProductDetail', async(req,res)=>{
    console.log(req.body);
    const {_id} = req.body
    Product.find({_id:_id}, function(err,products){
        if(err){throw err;}
        res.json(products);
    });


});

router.get('/getUsers', async(req,res)=>{
    console.log(req.body);
    
    User.find(function(err,users){
        if(err){throw err;}
        console.log(users);
        res.json(users);
    });

});

router.post('/deleteUser', async(req,res)=>{
    console.log(req.body);
    
    const {_id} = req.body
    //const user = await User.findOne({ _id });
    try {
            await User.deleteOne({_id})
            res.send({msg:"Done"});
    } catch (error) {
        res.status(422).send(error.message);
    }

});


router.post('/getAllUserProducts', async(req,res)=>{
    console.log(req.body);
    const {_user} = req.body
    Product.find({_user:_user}, function(err,products){
        if(err){throw err;}
        res.json(products);
    });


});

router.post('/search', async(req,res)=>{
    console.log(req.body);
    const {term} = req.body;
    
    Product.find(
       { $text: { $search: term }}
    , function(err,products){
        if(err){throw err;}
        res.json(products);
    });


});

//Exporter
module.exports = router;