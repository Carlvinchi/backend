const mongoose = require ('mongoose'); 
const Schema = mongoose.Schema;
    const productSchema =  new mongoose.Schema({

        category:{
            type:String,
            require:true,
            trim: true
        },
        title:{
            type:String,
            require:true,
            trim: true
        },
       
        description:{
            type:String,
            require:true,
            trim: true
        },
        price:{
            type:String,
            require:true
        },
        images:{
            type:String
        },
        contact:{
            type:String,
            require:true,
            trim: true
        },
        whatsapp:{
            type:String,
            require:true,
            trim: true
        },
        dateCreated:{
            type:Date
        },
        _user:{
            type:Schema.Types.ObjectId,
            ref:'User'
        }
     
});

productSchema.index({ '$**': 'text' });

mongoose.model('Product', productSchema);

 /* 
        category:{
            type:String,
            require:true
        },
        title:{
            type:String,
            require:true
        },
       
        description:{
            type:String,
            require:true
        },
        price:{
            type:String,
            require:true
        },
        images:{
            type:String
        },
        contact:{
            type:String,
            require:true
        },
        dateCreated:{
            type:Date
        },
        _user:{
            type:Schema.Types.ObjectId,
            ref:'User'
        }
        */